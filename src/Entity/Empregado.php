<?php

namespace App\Entity;

use App\Repository\EmpregadoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmpregadoRepository::class)]
class Empregado
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;


    #[ORM\Column(length: 255)]
    private ?string $nome = null;

    #[ORM\Column(length: 9)]
    private ?string $dni = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $apelidos = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $data_nacemento = null;

    #[ORM\Column(length: 50)]
    private ?string $categoria = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getApelidos(): ?string
    {
        return $this->apelidos;
    }

    public function setApelidos(?string $apelidos): self
    {
        $this->apelidos = $apelidos;

        return $this;
    }

    public function getDataNacemento(): ?\DateTimeInterface
    {
        return $this->data_nacemento;
    }

    public function setDataNacemento(\DateTimeInterface $data_nacemento): self
    {
        $this->data_nacemento = $data_nacemento;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }
}
