<?php

namespace App\Repository;

use App\Entity\Empregado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Empregado>
 *
 * @method Empregado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Empregado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Empregado[]    findAll()
 * @method Empregado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpregadoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Empregado::class);
    }

    public function save(Empregado $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Empregado $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function findByNome($nome): array
    {
       return $this->createQueryBuilder('e')
           ->andWhere('e.nome = :val')
           ->setParameter('val', $nome)
           ->orderBy('e.id', 'ASC')
           ->setMaxResults(20)
           ->getQuery()
           ->getResult()
       ;
    }

//    /**
//     * @return Empregado[] Returns an array of Empregado objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Empregado
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
