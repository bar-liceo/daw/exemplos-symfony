<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Persistence\ManagerRegistry;


use App\Entity\Empregado;
use App\Repository\EmpregadoRepository;
use App\Form\EmpregadoType;

class LuckyController extends AbstractController
{
    const palabras = array(
        "Fernando",
        "Hugo",
        "Alberto",
        "Rubén",
        "Kike",
        "Gustavo",
        "Javi",
        "Martín",
        "Ángel",
        "David");

    #[Route('/lucky/n', name: 'lucky_number')]
    public function number(): Response
    {
        $number = random_int(0, 3);
        if ($number % 2 == 0){
            $par = true;
        } else {
            $par = false;
        }

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
            'par' => $par,
        ]);
    }

    #[Route('/lucky/word', name:'lucky_word')]
    public function word(): Response
    {
        $numero = random_int(0, sizeof($this::palabras)-1);

        return $this->render(
            'lucky/word.html.twig',
            [
                'palabra' => $this::palabras[$numero],
                'alumnos' => $this::palabras,
            ]);
    }
    #[Route('/lucky/word/{indice}', requirements: ['indice' => '\d+'], name:'lucky_word_i')]
    public function wordIndex(int $indice): Response
    {
        if ($indice >= 0 && $indice < sizeof($this::palabras)){
            return $this->render(
                'lucky/word.html.twig',
                [
                    'palabra' => $this::palabras[$indice],
                ]);
        }

        return $this->render(
            'lucky/error.html.twig',
            [
                'msgError' => "O número ten que estar entre 0 e ".sizeof($this::palabras),
                
            ]);
        
    }

    #[Route('/lucky/ola/{nome}', requirements: ['nome' => '\w+'], name: 'lucky_ola')]
    public function ola(string $nome): Response
    {
        return $this->render(
            'lucky/ola.html.twig',
            [
                'nome' => $nome,
            ]
            );
    }
    #[Route('/', name: 'index')]
    public function principal(): Response
    {
        return $this->render('principal.html.twig',[]);
    }

    #[Route('/empregado/list', name: 'list_empregado')]
    public function listarEmpregado(EmpregadoRepository $empregadoRepository): Response
    {
        $empregados = $empregadoRepository->findAll();

        return $this->render('empregado/lista.html.twig', [
            "empregados" => $empregados,
        ]);
    }

    #[Route('/empregado/crear', name: 'crear_empregado')]
    public function crearEmpregado(ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        
        $dataNacemento = \DateTime::createFromFormat('Y-m-d', "1980-10-12"); 

        $empregado = new Empregado();
        $empregado->setNome("Pepito");
        $empregado->setDNI("11122233K");
        $empregado->setApelidos("López");
        $empregado->setDataNacemento($dataNacemento);
        $empregado->setCategoria("A1");

        $entityManager->persist($empregado);
        $entityManager->flush();

        return new Response('Saved new Empregado with id '.$empregado->getId());
    }

    #[Route('/empregado/novo', name: 'novo_empregado')]
    public function novoEmpregado(ManagerRegistry $doctrine, Request $request): Response
    {
        $empregado = new Empregado();

        $form = $this->createForm(EmpregadoType::class, $empregado);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $empregado = $form->getData();

            // gravar empregado
            $doctrine->getManager()->persist($empregado);
            $doctrine->getManager()->flush();

            //redireccionamos directamente á lista de empregados:
            return $this->redirectToRoute('list_empregado');

            // Se quixeramos mostrar feedback de que se creou noutra vista distinta, podiamos facer así:
            //return $this->render('empregado/creado.html.twig');
        }

        
        return $this->render('empregado/form_new.html.twig', [
            "form" => $form,
        ]);
    }

}