<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EmpregadoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nome', TextType::class)
            ->add('dni', TextType::class)
            ->add('apelidos', TextType::class)
            ->add('dataNacemento', DateType::class)
            ->add('categoria', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Agregar empregado'])
        ;
    }
}

?>